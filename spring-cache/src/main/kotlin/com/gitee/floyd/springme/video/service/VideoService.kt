package com.gitee.floyd.springme.video.service

import com.gitee.floyd.springme.video.pojo.FullVideo
import com.gitee.floyd.springme.video.repo.VideoRelatedRepo
import org.springframework.stereotype.Service

/**
 * @Author gd
 * @Date 2021/9/27 2:50 下午
 */
@Service
class VideoService(private val repo: VideoRelatedRepo) {

    fun listVideo(ids: List<String>): List<FullVideo> {
        val videos = ids.map { repo.getVideo(it) }
        val videoPlayInfos = repo.listVideoPlayInfo(ids).groupBy { it.videoId }
        return videos.map { video ->
            FullVideo(video, videoPlayInfos[video.id]!!)
        }
    }

}
