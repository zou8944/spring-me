package com.gitee.floyd.springme.video

/**
 * @Author gd
 * @Date 2021/9/27 2:53 下午
 */
const val REDIS_CACHE_NAME_VIDEO = "VIDEO"
const val REDIS_CACHE_NAME_PLAY_INFO = "PLAY_INFO"
const val REDIS_CACHE_PREFIX = "SPRING_DEMO_VIDEO"
const val REDIS_CACHE_EXPIRE_DAYS = 1L

