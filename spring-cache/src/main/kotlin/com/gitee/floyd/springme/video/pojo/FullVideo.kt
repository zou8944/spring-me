package com.gitee.floyd.springme.video.pojo

import com.gitee.floyd.springme.video.repo.entity.Video
import com.gitee.floyd.springme.video.repo.entity.VideoPlayInfo

/**
 * @Author gd
 * @Date 2021/9/27 3:16 下午
 */
data class FullVideo(
    private val video: Video,
    private val playInfos: List<VideoPlayInfo>
)
