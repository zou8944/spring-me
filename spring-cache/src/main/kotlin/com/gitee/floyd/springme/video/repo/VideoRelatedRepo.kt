package com.gitee.floyd.springme.video.repo

import com.gitee.floyd.springme.video.repo.entity.Video
import com.gitee.floyd.springme.video.repo.entity.VideoPlayInfo

/**
 * @Author gd
 * @Date 2021/9/27 2:58 下午
 */
interface VideoRelatedRepo {

    fun addVideo(video: Video): Boolean

    fun getVideo(id: String): Video

    fun updateVideo(video: Video): Video

    fun deleteVideo(id: String): Video

    fun addVideoPlayInfo(videoPlayInfo: VideoPlayInfo): Boolean

    fun listVideoPlayInfo(videoIds: List<String>): List<VideoPlayInfo>

    fun updateVideoPlayInfo(videoPlayInfo: VideoPlayInfo): VideoPlayInfo

    fun deleteVideoPlayInfo(id: String): VideoPlayInfo

}
