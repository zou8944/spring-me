package com.gitee.floyd.springme.video.repo.entity

import java.io.Serializable

/**
 * @Author gd
 * @Date 2021/9/27 2:48 下午
 */
data class VideoPlayInfo(
    val id: String,
    val videoId: String,
    val playUrl: String,
    val source: String
)
