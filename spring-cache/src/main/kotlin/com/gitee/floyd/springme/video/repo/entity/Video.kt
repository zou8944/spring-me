package com.gitee.floyd.springme.video.repo.entity

import java.io.Serializable

/**
 * @Author gd
 * @Date 2021/9/27 2:48 下午
 */
data class Video(
    val id: String,
    val title: String,
    val description: String,
    val coverUrl: String
)
