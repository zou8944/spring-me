package com.gitee.floyd.springme

import com.gitee.floyd.springme.video.repo.VideoRelatedRepo
import com.gitee.floyd.springme.video.repo.entity.Video
import com.gitee.floyd.springme.video.repo.entity.VideoPlayInfo
import com.gitee.floyd.springme.video.service.VideoService
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class SpringMeApplicationTests {

    @Autowired
    private lateinit var repo: VideoRelatedRepo
    @Autowired
    private lateinit var videoService: VideoService

    private val demoVideoId = "video1"

    @BeforeEach
    fun init() {
        val video1 = Video(demoVideoId, "标题", "描述", "https://www.baidu.com/image/lalala.png")
        val playInfo1 = VideoPlayInfo("1", demoVideoId, "https://weawegawg.mp4", "iqiyi")
        val playInfo2 = VideoPlayInfo("2", demoVideoId, "https://awegawhag.mp4", "tencent")
        repo.addVideo(video1)
        repo.addVideoPlayInfo(playInfo1)
        repo.addVideoPlayInfo(playInfo2)
    }

    @Test
    fun test() {
        // 第一遍获取，应该不会走缓存，可以观察控制台输出
        videoService.listVideo(listOf(demoVideoId)).let { println(it) }
        // 第二遍获取，应该会走缓存，可以观察控制台输出
        videoService.listVideo(listOf(demoVideoId)).let { println(it) }
    }

//    @AfterEach
    fun destroy() {
        repo.deleteVideo(demoVideoId)
        repo.deleteVideoPlayInfo("1")
        repo.deleteVideoPlayInfo("2")
    }

}
